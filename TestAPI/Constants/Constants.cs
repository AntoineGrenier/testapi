﻿namespace TestAPI.Constants
{
    /// <summary>
    ///     Name constants
    /// </summary>
    public static class Constants
    {
        /// <summary>
        ///     The fr code from the frontend
        /// </summary>
        public const string FR = "fr";

        /// <summary>
        ///     The error message
        /// </summary>
        public const string ERROR_MESSAGE = "error_message:";
    }
}