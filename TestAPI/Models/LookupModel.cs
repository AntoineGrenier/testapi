﻿using System.Collections.Generic;

namespace TestAPI.Models
{
    /// <summary>
    /// </summary>
    public class LookupModel
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LookupModel" /> class.
        /// </summary>
        public LookupModel()
        {
            Title = string.Empty;
            Items = new List<LookupItemModel>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="LookupModel" /> class.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="items">The items.</param>
        public LookupModel(string title, ICollection<LookupItemModel> items)
        {
            Title = title;
            Items = items;
        }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title { get; set; }

        /// <summary>
        ///     Gets or sets the items.
        /// </summary>
        /// <value>
        ///     The items.
        /// </value>
        public ICollection<LookupItemModel> Items { get; set; }
    }

    /// <summary>
    /// </summary>
    public class LookupItemModel
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="LookupItemModel" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="filterTitle">The filter title.</param>
        /// <param name="order">The order.</param>
        public LookupItemModel(string value, string filterTitle, int order)
        {
            Title = filterTitle;
            Value = value;
            Order = order;
        }

        /// <summary>
        ///     Gets the title.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title { get; }

        /// <summary>
        ///     Gets the value.
        /// </summary>
        /// <value>
        ///     The value.
        /// </value>
        public string Value { get; }

        /// <summary>
        ///     Gets the order.
        /// </summary>
        /// <value>
        ///     The order.
        /// </value>
        public int Order { get; }
    }
}