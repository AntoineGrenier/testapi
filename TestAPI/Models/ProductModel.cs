﻿using System.Collections.Generic;
using TestAPI.Entities;
using TestAPI.Service;

namespace TestAPI.Models
{
    /// <summary>
    ///     Product model
    /// </summary>
    /// <seealso cref="TestAPI.Service.IIdentifiable" />
    public class ProductModel : IIdentifiable
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ProductModel"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public ProductModel(Product entity)
        {
            IdNumber = entity.IdNumber;
            Slug = entity.Slug;
            Status = 1;
            Images = new List<string>
            {
                "images1.jpg"
            };
            Title = "Product title";
            Price = entity.Price;
            InStock = 1000;
        }

        /// <summary>
        ///     Gets the title.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title { get; }

        /// <summary>
        ///     Gets the slug.
        /// </summary>
        /// <value>
        /// The slug.
        /// </value>
        public string Slug { get; }


        /// <summary>
        ///     Gets the status.
        /// </summary>
        /// <value>
        ///     The status.
        /// </value>
        public int Status { get; }

        /// <summary>
        ///     Gets or sets the images.
        /// </summary>
        /// <value>
        ///     The images.
        /// </value>
        public ICollection<string> Images { get; set; }

        /// <summary>
        ///     Gets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        public decimal Price { get; }

        /// <summary>
        ///     Gets the in stock.
        /// </summary>
        /// <value>
        ///     The in stock.
        /// </value>
        public decimal? InStock { get; }

        /// <summary>
        ///     Gets or sets the identifier number.
        /// </summary>
        /// <value>
        ///     The identifier number.
        /// </value>
        /// <inheritdoc />
        public int IdNumber { get; set; }

        //Other properties cover for intellectual property
    }

}