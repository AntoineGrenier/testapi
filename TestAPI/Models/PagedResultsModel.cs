﻿using System.Collections.Generic;

namespace TestAPI.Models
{
    /// <summary>
    ///     Paged results
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PagedResultsModel<T>
    {
        /// <summary>
        ///     The page number this page represents.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        ///     The size of this page.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        ///     The total number of pages available.
        /// </summary>
        public int TotalNumberOfPages { get; set; }

        /// <summary>
        ///     The total number of records available.
        /// </summary>
        public int TotalNumberOfRecords { get; set; }

        /// <summary>
        ///     Gets or sets the next page. If null, not more page
        /// </summary>
        /// <value>
        ///     The next page.
        /// </value>
        public int? NextPage { get; set; }

        /// <summary>
        ///     Gets or sets the previous page. If null, first page
        /// </summary>
        /// <value>
        ///     The next page.
        /// </value>
        public int? PreviousPage { get; set; }

        /// <summary>
        ///     Gets or sets the filters.
        /// </summary>
        /// <value>
        ///     The filters.
        /// </value>
        public ICollection<LookupModel> Filters { get; set; }

        /// <summary>
        ///     The records this page represents.
        /// </summary>
        public IEnumerable<T> Results { get; set; }
    }
}