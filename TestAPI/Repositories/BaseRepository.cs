﻿using TestAPI.Entities.Context;

namespace TestAPI.Repositories
{
    /// <summary>
    ///     Base repository
    /// </summary>
    public abstract class BaseRepository
    {
        /// <summary>
        ///     The context
        /// </summary>
        protected readonly IDatabaseContext _context;


        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseRepository" /> class.
        /// </summary>
        /// <param name="context">The DB context.</param>
        protected BaseRepository(IDatabaseContext context)
        {
            _context = context;
        }
    }
}