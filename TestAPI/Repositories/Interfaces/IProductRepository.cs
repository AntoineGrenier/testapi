﻿using System.Threading.Tasks;
using TestAPI.Models;
using TestAPI.Service;

namespace TestAPI.Repositories.Interfaces
{
    /// <summary>
    ///     Quote repository interface
    /// </summary>
    public interface IProductRepository
    {
        /// <summary>
        /// Gets a paged product list with the selected parameters
        /// </summary>
        /// <param name="searchTerms">The search terms.</param>
        /// <param name="language">The language. (optional, FR by default)</param>
        /// <param name="page">The number of the selected page. (optional, 1 by default)</param>
        /// <param name="pageSize">Size of the page.  (optional, 10 by default)</param>
        /// <returns>Paged products model list</returns>
        Task<OperationResult<PagedResultsModel<ProductModel>>> Get(string searchTerms = "",
            string language = Constants.Constants.FR, int? page = null, int pageSize = 10);

        /// <summary>
        ///     Gets the product with is slug.
        /// </summary>
        /// <param name="slug">The slug.</param>
        /// <param name="language">The language. (FR by default)</param>
        /// <returns>The selected product model</returns>
        Task<OperationResult<ProductModel>> Get(string slug, string language = Constants.Constants.FR);

        /// <summary>
        /// Sets the specified product model in the DB.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>The inserted product in the DB</returns>
        Task<OperationResult<ProductModel>> Set(ProductModel model);
    }
}