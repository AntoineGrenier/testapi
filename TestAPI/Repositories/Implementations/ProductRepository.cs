﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestAPI.Converters;
using TestAPI.Entities;
using TestAPI.Entities.Context;
using TestAPI.Models;
using TestAPI.Repositories.Interfaces;
using TestAPI.Service;

namespace TestAPI.Repositories.Implementations
{
    /// <summary>
    ///     Product repository
    /// </summary>
    /// <seealso cref="IProductRepository" />
    public class ProductRepository : BaseRepository, IProductRepository
    {
        /// <inheritdoc />
        public ProductRepository(DatabaseContext context) : base(context){}

        /// <inheritdoc />
        public Task<OperationResult<PagedResultsModel<ProductModel>>> Get(string searchTerms = "",
            string language = Constants.Constants.FR, int? page = null, int pageSize = 10)
        {
            return Task.Run(() =>
            {
                try
                {
                    //Get default product
                    IQueryable<Product> products = _context.Products
                        .Include(x => x.ParentProduct)
                        .Include(x => x.ChildrenProducts);

                    //Other intellectual property process

                    //Search in all the string columns of the product DB
                    if (searchTerms != string.Empty)
                    {
                        var upperSearchTerms = searchTerms.ToUpper();
                        products = products.Where(s => s.DescFr.ToUpper().Contains(upperSearchTerms) || s.DescEn.ToUpper().Contains(upperSearchTerms));
                    }
                    //Get total number of record
                    var totalNumberOfRecords = products.Count();

                    //Get the rest of the page
                    var mod = totalNumberOfRecords % pageSize;

                    //Get the number of pages
                    var totalPageCount = totalNumberOfRecords / pageSize + (mod == 0 ? 0 : 1);

                    //Skip and take product depending on the parameters
                    page = page ?? 1;
                    products = products.Skip(pageSize * ((int)page - 1)).Take(pageSize);

                    //Next page
                    var nextPage = page == totalPageCount ? null : page + 1;

                    //Previous page
                    var previousPage = page == 1 ? null : page - 1;

                    //Other intellectual property process

                    //Build the product models list
                    var results = new List<ProductModel>(
                    new ProductConverter().ToModels(products));

                    //Other intellectual property process

                    //Return the paged product
                    return new OperationResult<PagedResultsModel<ProductModel>>(
                        new PagedResultsModel<ProductModel>
                        {
                            Results = results,
                            PageNumber = (int) page,
                            PageSize = results.Count,
                            TotalNumberOfPages = totalPageCount,
                            TotalNumberOfRecords = totalNumberOfRecords,
                            NextPage = nextPage,
                            PreviousPage = previousPage,
                            Filters = null
                        });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return new OperationResult<PagedResultsModel<ProductModel>>(
                        new ValidationItem(Constants.Constants.ERROR_MESSAGE, e.Message));
                }
            });
        }

        /// <inheritdoc />
        public Task<OperationResult<ProductModel>> Get(string slug, string language = Constants.Constants.FR)
        {
            return Task.Run(() =>
            {
                try
                {
                    //Get the unique product
                    var product = _context.Products.FirstOrDefault(x => x.Slug == slug);

                    //Other intellectual property process

                    //Convert entity to model
                    return new OperationResult<ProductModel>(new ProductConverter().ToModel(product));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return new OperationResult<ProductModel>(new ValidationItem(Constants.Constants.ERROR_MESSAGE, e.Message));
                }
            });
        }

        /// <inheritdoc />
        public Task<OperationResult<ProductModel>> Set(ProductModel model)
        {
            return Task.Run(() =>
            {
                try
                {
                    //Other intellectual property process

                    //Convert model to entity
                    var converter = new ProductConverter();
                    var product = converter.ToEntity(model);

                    //Other intellectual property process

                    //Add the product to the DB
                    _context.Products.Add(product);

                    //Save the product
                    _context.SaveChanges();

                    //Other intellectual property process

                    //Convert newly add entity to model
                    return new OperationResult<ProductModel>(converter.ToModel(product));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return new OperationResult<ProductModel>(new ValidationItem(Constants.Constants.ERROR_MESSAGE, e.Message));
                }
            });
        }
    }
}