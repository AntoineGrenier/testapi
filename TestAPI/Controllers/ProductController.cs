﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestAPI.Models;
using TestAPI.Repositories.Interfaces;
using TestAPI.Service;

namespace TestAPI.Controllers
{
    /// <inheritdoc />
    [Route("api/products")]
    [Authorize]
    public class ProductController : Controller
    {
        private readonly IProductRepository _repo;

        /// <inheritdoc />
        public ProductController(IProductRepository repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Gets a paged product list with the selected parameters
        /// </summary>
        /// <param name="searchTerms">The search terms.</param>
        /// <param name="language">The language. (optional, FR by default)</param>
        /// <param name="page">The number of the selected page. (optional, null by default)</param>
        /// <param name="pageSize">Size of the page.  (optional, 10 by default)</param>
        /// <returns>Paged products model list</returns>
        [HttpGet]
        [Route("get")]
        [AllowAnonymous]
        public Task<OperationResult<PagedResultsModel<ProductModel>>> Get(string searchTerms = "",
            string language = Constants.Constants.FR, int? page = null, int pageSize = 10)
        {
            return _repo.Get(searchTerms, language, page, pageSize);
        }

        /// <summary>
        ///     Gets the product with is slug.
        /// </summary>
        /// <param name="slug">The slug.</param>
        /// <param name="language">The language. (FR by default)</param>
        /// <returns>The selected product model</returns>
        [HttpGet]
        [Route("get/{slug}")]
        [AllowAnonymous]
        public Task<OperationResult<ProductModel>> Get(string slug, string language = Constants.Constants.FR)
        {
            return _repo.Get(slug, language);
        }


        /// <summary>
        /// Sets the specified product model in the DB.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>The inserted product in the DB</returns>
        [HttpPost]
        [Route("set")]
        [Authorize(Roles = "admin")]
        public Task<OperationResult<ProductModel>> Set([FromBody] ProductModel model)
        {
            return _repo.Set(model);
        }
    }
}