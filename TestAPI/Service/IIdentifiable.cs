﻿namespace TestAPI.Service
{
    /// <summary>
    ///     ID interface
    /// </summary>
    public interface IIdentifiable
    {
        /// <summary>
        ///     Gets or sets the identifier number.
        /// </summary>
        /// <value>
        ///     The identifier number.
        /// </value>
        int IdNumber { get; set; }
    }
}