﻿namespace TestAPI.Service
{
    /// <summary>
    ///     Return data from a API call
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OperationResult<T>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="OperationResult{T}" /> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public OperationResult(T entity)
        {
            Validation = new ValidationResults();
            Result = entity;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="OperationResult{T}" /> class.
        /// </summary>
        /// <param name="item">The item.</param>
        public OperationResult(ValidationItem item)
        {
            Validation = new ValidationResults(item);
        }

        /// <summary>
        ///     Gets the result.
        /// </summary>
        /// <value>
        ///     The result.
        /// </value>
        public T Result { get; }

        /// <summary>
        ///     Gets or sets the validation.
        /// </summary>
        /// <value>
        ///     The validation.
        /// </value>
        public ValidationResults Validation { get; set; }
    }

    /// <summary>
    ///     Validation result object
    /// </summary>
    public class ValidationResults
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ValidationResults" /> class.
        /// </summary>
        public ValidationResults()
        {
            Result = new ValidationItem();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ValidationResults" /> class.
        /// </summary>
        /// <param name="item">The item.</param>
        public ValidationResults(ValidationItem item)
        {
            Result = item;
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is successful.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is successful; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccessful
        {
            get => Result.Message == string.Empty;
            private set { }
        }

        /// <summary>
        ///     Gets or sets the result.
        /// </summary>
        /// <value>
        ///     The result.
        /// </value>
        public ValidationItem Result { get; set; }
    }

    /// <summary>
    ///     Validation item object
    /// </summary>
    public class ValidationItem
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ValidationItem" /> class.
        /// </summary>
        public ValidationItem()
        {
            Message = string.Empty;
            Code = "200";
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ValidationItem" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="property">The property.</param>
        public ValidationItem(string message, string property = null)
        {
            Message = message;
            Code = property;
        }

        /// <summary>
        ///     Gets or sets the message.
        /// </summary>
        /// <value>
        ///     The message.
        /// </value>
        public string Message { get; set; }

        /// <summary>
        ///     Gets or sets the code.
        /// </summary>
        /// <value>
        ///     The code.
        /// </value>
        public string Code { get; set; }
    }
}