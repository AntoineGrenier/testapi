﻿using System.Collections.Generic;
using System.Linq;
using TestAPI.Converters.Interface;
using TestAPI.Entities;
using TestAPI.Models;

namespace TestAPI.Converters
{
    /// <summary>
    ///     Product converter
    /// </summary>
    public class ProductConverter : IConverter<Product, ProductModel>
    {
        /// <inheritdoc />
        public Product ToEntity(ProductModel model)
        {
            //Other intellectual property process

            return model != null
                ? new Product
                {
                    IdNumber = model.IdNumber,
                    ParentIdNumber = null,
                    Slug = model.Slug,
                    DescFr = "blablabla",
                    DescEn = "blobloblo",
                    Price = model.Price
                }
                : null;
        }

        /// <inheritdoc />
        public ProductModel ToModel(Product entity)
        {
            return entity != null ? new ProductModel(entity) : null;
        }

        /// <inheritdoc />
        public IEnumerable<ProductModel> ToModels(IQueryable<Product> entities)
        {
            var models = new List<ProductModel>();
            foreach (var entity in entities) models.Add(ToModel(entity));
            return models;
        }
    }
}