﻿using System.Collections.Generic;
using System.Linq;

namespace TestAPI.Converters.Interface
{
    /// <summary>
    ///     Interface that every converters must implement
    /// </summary>
    /// <typeparam name="TEntity">The type of the dto.</typeparam>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public interface IConverter<TEntity, TModel>
    {
        /// <summary>
        ///     To the entity.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        TEntity ToEntity(TModel model);

        /// <summary>
        ///     To the model.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        TModel ToModel(TEntity entity);

        /// <summary>
        ///     To the models.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <returns></returns>
        IEnumerable<TModel> ToModels(IQueryable<TEntity> entities);
    }
}