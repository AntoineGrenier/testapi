using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TestAPI.Service;

namespace TestAPI.Entities
{
    /// <summary>
    /// </summary>
    /// <seealso cref="TestAPI.Service.IIdentifiable" />
    [Table("Item")]
    public class Product : IIdentifiable
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Product" /> class.
        /// </summary>
        public Product()
        {
            ChildrenProducts = new List<Product>();
            ParentProduct = new Product();
        }

        /// <summary>
        ///     Gets or sets the identifier number.
        /// </summary>
        /// <value>
        ///     The identifier number.
        /// </value>
        [Key]
        public int IdNumber { get; set; }

        /// <summary>
        ///     Gets or sets the parent identifier number.
        /// </summary>
        /// <value>
        ///     The parent identifier number.
        /// </value>
        public int? ParentIdNumber { get; set; }

        /// <summary>
        ///     Gets or sets the slug. (Number of the product)
        /// </summary>
        /// <value>
        ///     The slug.
        /// </value>
        [StringLength(20)]
        public string Slug { get; set; }

        /// <summary>
        ///     Gets or sets the desc fr.
        /// </summary>
        /// <value>
        ///     The desc fr.
        /// </value>
        [StringLength(50)]
        public string DescFr { get; set; }

        /// <summary>
        ///  Gets or sets the desc en.
        /// </summary>
        /// <value>
        ///  The desc en.
        /// </value>
        [StringLength(50)]
        public string DescEn { get; set; }

        /// <summary>
        ///     Gets or sets the price.
        /// </summary>
        /// <value>
        ///     The price.
        /// </value>
        public decimal Price { get; set; }

        /// <summary>
        ///     Gets or sets the children products.
        /// </summary>
        /// <value>
        /// The children products.
        /// </value>
        public virtual ICollection<Product> ChildrenProducts { get; set; }

        /// <summary>
        ///  Gets or sets the parent product.
        /// </summary>
        /// <value>
        ///     The parent product.
        /// </value>
        public virtual Product ParentProduct { get; set; }


        //Other properties cover for intellectual property
    }
}