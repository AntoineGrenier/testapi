﻿using Microsoft.EntityFrameworkCore;

namespace TestAPI.Entities.Context
{
    /// <summary>
    ///     Interface to link the DB to the middle tier
    /// </summary>
    public interface IDatabaseContext
    {

        /// <summary>
        ///     Gets or sets the products.
        /// </summary>
        /// <value>
        ///     The products.
        /// </value>
        DbSet<Product> Products { get; set; }

        /// <summary>
        ///     Saves the changes.
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
    }
}